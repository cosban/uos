package action

type Action int

const (
	ECHO Action = iota
	PRELOAD
	PLAY
	DISPLAY
	CLEAR
	TOGGLEOBFUSCATION
)
