package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"

	"gitlab.com/cosban/config"
	"gitlab.com/cosban/uos"
	"gitlab.com/cosban/uos/internal"
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

func init() {
	if err := config.ReadConfig(&internal.Config); err != nil {
		log.Fatal("[UOS] unable to read config file", err)
	}
}

func main() {
	os := uos.NewOS(internal.Config.Root)

	go os.Run()
	http.HandleFunc("/uos", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		terminal := uos.NewTerminal(conn, os)

		os.Register(terminal)
	})
	log.Println("Ready")
	err := http.ListenAndServe(fmt.Sprintf("%s:%d", internal.Config.Host, internal.Config.Port), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
