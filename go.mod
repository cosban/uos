module gitlab.com/cosban/uos

go 1.12

require (
	github.com/gorilla/websocket v1.4.0
	github.com/mongodb/mongo-go-driver v1.0.3 // indirect
	gitlab.com/cosban/config v0.0.0-20181013175647-04c8029c149f
)
