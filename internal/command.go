package internal

import "gitlab.com/cosban/uos/action"

type Command struct {
	Action action.Action `json:"action"`
	Value  string        `json:"value"`
}
