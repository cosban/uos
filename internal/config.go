package internal

type Configuration struct {
	Root             string `json:"root"`
	Host             string `json:"host"`
	Port             int    `json:"port"`
	TerminalUser     string `json:"user"`
	TerminalPassword string `json:"password"`
}

var Config Configuration
