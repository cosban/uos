package internal

import "gitlab.com/cosban/uos/action"

type Terminal interface {
	Command(action.Action, string) Command
	Send(action.Action, string)
	SendActions(...action.Action)
	SendCommands(commands ...Command)
	CWD() (string, []string)
	Chdir([]string)
	LocalRoot() string
	Path() []string
	AddState(int32)
	RemoveState(int32)
}
