package program

import (
	"gitlab.com/cosban/uos/internal"
)

type Runner func([]string, internal.Terminal)

type Program interface {
	Synopsis() string
	Description() string
	Run([]string, internal.Terminal)
}

type program struct {
	synopsis, description string
	runner                Runner
}

func New(synopsis, description string, runner Runner) Program {
	return &program{
		synopsis:    synopsis,
		description: description,
		runner:      runner,
	}
}

func (c *program) Synopsis() string {
	return c.synopsis
}

func (c *program) Description() string {
	return c.description
}

func (c *program) Run(args []string, terminal internal.Terminal) {
	c.runner(args, terminal)
}
