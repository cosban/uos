package program

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"plugin"
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
)

var programsByLocation = map[string]map[string]Program{}

func GetProgramsAtLocation(location string) []Program {
	located := programsByLocation[location]
	programs := []Program{}
	for _, program := range located {
		programs = append(programs, program)
	}
	return programs
}

func GetProgramAtLocation(location, name string) Program {
	return programsByLocation[location][name]
}

func GetProgram(cwd, name string, path []string) Program {
	if localProgram := GetProgramAtLocation(cwd, name); localProgram != nil {
		return localProgram
	}
	for _, location := range path {
		if program := GetProgramAtLocation(location, name); program != nil {
			return program
		}
	}
	return nil
}

func RegisterProgramsOnPath(localroot string, paths ...string) {
	log.Println("localroot is", localroot)
	for _, path := range paths {
		RegisterPrograms(localroot, path)
	}
}

func RegisterPrograms(localroot, path string) {
	files, err := ioutil.ReadDir(localroot + path)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, f := range files {
		command := strings.TrimSuffix(f.Name(), ".osc")
		if isShellCommand(f) && !isRegistered(path, command) {
			log.Println("found plugin at", path+string(os.PathSeparator)+f.Name())
			program, err := plugin.Open(localroot + path + string(os.PathSeparator) + f.Name())
			if err != nil {
				log.Fatal(err)
			}
			constructor, err := program.Lookup("New")
			Register(path, command, constructor.(func() Program)())
		}
	}
}

func isShellCommand(file os.FileInfo) bool {
	return !file.IsDir() && file.Mode()|0111 != 0 && strings.HasSuffix(file.Name(), ".osc")
}

func isRegistered(path, filename string) bool {
	_, ok := programsByLocation[path][filename]
	return ok
}

func Register(path, command string, program Program) {
	if _, ok := programsByLocation[path]; !ok {
		programsByLocation[path] = map[string]Program{}
	}
	programsByLocation[path][command] = program
}

func RunProgram(input string, terminal internal.Terminal) {
	split := strings.Split(strings.Trim(input, " "), " ")
	command, args := split[0], split[1:]
	cwd, _ := terminal.CWD()
	if program := GetProgram(cwd, strings.ToLower(command), terminal.Path()); program != nil {
		program.Run(args, terminal)
	} else {
		terminal.Send(action.ECHO, fmt.Sprintf("command not found: %s", command))
	}
}
