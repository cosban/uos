# Hold Up

These files are packaged with the code, but are actually runtime dependencies which allow the terminal to run commands and other arbitrary code.

If you want to be able to run/build a command, it will need to be built using golang's plugin system as follows:

```bash
go build -buildmode=plugin -o=<command>.osc
```

commands can only be executed if they have an osc (Operating System Command) extension.
