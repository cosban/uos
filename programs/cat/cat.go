package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "concatenate files and print them on the standard output"
	DESCRIPTION = `Concatenate FILE(s) to standard output.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	if len(args) < 1 {
		terminal.Send(action.ECHO, "The syntax of the command is incorrect.")
	}
	localroot, err := filepath.Abs(terminal.LocalRoot())
	if err != nil {
		log.Println(err)
		terminal.Send(action.ECHO, fmt.Sprintf("cat: %s: No such file or directory", args[0]))
	}
	for _, arg := range args {
		file := mapPath(arg, terminal)
		absfile, err := filepath.Abs(file)
		if err != nil || !strings.HasPrefix(absfile, localroot) {
			terminal.Send(action.ECHO, fmt.Sprintf("cat: %s: No such file or directory", arg))
			if err != nil {
				log.Println(err)
			}
		} else {
			info, err := os.Lstat(absfile)
			if err != nil {
				terminal.Send(action.ECHO, fmt.Sprintf("cat: %s: No such file or directory", arg))
			} else if info.IsDir() {
				terminal.Send(action.ECHO, fmt.Sprintf("cat: %s: Is a directory", arg))
			} else {
				catFile(arg, absfile, terminal)
			}
		}
	}
}

func mapPath(argument string, terminal internal.Terminal) string {
	if strings.HasPrefix(argument, "/") {
		return terminal.LocalRoot() + argument
	}
	cwd, _ := terminal.CWD()
	return terminal.LocalRoot() + cwd + string(os.PathSeparator) + argument
}

func catFile(arg, location string, terminal internal.Terminal) {
	file, err := os.Open(location)
	if err != nil {
		terminal.Send(action.ECHO, fmt.Sprintf("cat: %s: Permission Denied", arg))
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		terminal.Send(action.ECHO, scanner.Text())
	}
}
