package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "changes the current directory"
	DESCRIPTION = `changes the current directory

	..   Specifies that you want to change to the parent directory.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	if len(args) > 1 {
		terminal.Send(action.ECHO, "cd: too many arguments")
		return
	} else if len(args) == 0 {
		args = []string{""}
	}
	nodes := strings.Split(args[0], string(os.PathSeparator))
	_, cwd := terminal.CWD()
	for _, node := range nodes {
		if node == "" {
			continue
		}
		if node == ".." {
			cwd = pop(cwd)
		} else {
			var err error
			cwd, err = push(node, cwd, terminal)
			if err != nil {
				terminal.Send(action.ECHO, fmt.Sprintf("cd: %s: %s", args[0], err.Error()))
				return
			}
		}
	}
	terminal.Chdir(cwd)
	terminal.SendCommands()
}

func pop(slice []string) []string {
	if len(slice) == 0 {
		return slice
	}
	return slice[:len(slice)-1]
}

func push(node string, cwd []string, terminal internal.Terminal) ([]string, error) {
	prefix := strings.Join(append([]string{terminal.LocalRoot()}, cwd...), string(os.PathSeparator))
	info, err := os.Lstat(prefix + string(os.PathSeparator) + node)
	if err != nil {
		return nil, errors.New("No such file or directory")
	}
	if !info.IsDir() {
		return nil, errors.New("Not a directory")
	}
	return append(cwd, node), nil
}
