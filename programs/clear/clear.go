package main

import (
	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "clear the terminal screen"
	DESCRIPTION = `clear clears your screen if this is possible, including its scrollback buffer
	
clear ignores any command-line parameters that may be present.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	terminal.SendActions(action.CLEAR)
}
