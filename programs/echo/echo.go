package main

import (
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "display a line of text"
	DESCRIPTION = `Echo the STRING(s) to standard output.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	message := strings.Join(args, " ")
	terminal.Send(action.ECHO, message)
}
