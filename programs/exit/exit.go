package main

import (
	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "cause normal process termination"
	DESCRIPTION = `cause normal process termination`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	terminal.RemoveState(internal.UserProvided | internal.Authenticated)
	terminal.SendActions(action.CLEAR)
}
