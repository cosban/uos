package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "list the directory contents"
	DESCRIPTION = `List information about the FILEs (the current directory). Sorts entries alphabetically.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	cwd, _ := terminal.CWD()
	files, err := ioutil.ReadDir(terminal.LocalRoot() + cwd)
	if err != nil {
		terminal.Send(action.ECHO, "error listing files")
	}
	var buffer bytes.Buffer
	prefix := ""
	for _, f := range files {
		buffer.WriteString(prefix)
		nodes := strings.Split(f.Name(), string(os.PathSeparator))
		buffer.WriteString(nodes[len(nodes)-1])
		prefix = " "
	}
	terminal.Send(action.ECHO, buffer.String())
}
