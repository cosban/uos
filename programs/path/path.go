package main

import (
	"strings"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "print the full path"
	DESCRIPTION = `Print the full path.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	path := strings.Join(append([]string{"."}, terminal.Path()...), ";")
	terminal.Send(action.ECHO, path)
}
