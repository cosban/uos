package main

import (
	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	SYNOPSIS    = "print name of current/working directory"
	DESCRIPTION = `Print the full filename of the current working directory.`
)

func main() {}

func New() program.Program {
	return program.New(
		SYNOPSIS,
		DESCRIPTION,
		runner,
	)
}

func runner(args []string, terminal internal.Terminal) {
	cwd, _ := terminal.CWD()
	terminal.Send(action.ECHO, cwd)
}
