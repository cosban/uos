package uos

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gorilla/websocket"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWaiterm.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// terminal represents a single terminal session which a client has opened
type terminal struct {
	conn *websocket.Conn
	os   *UOS
	// Buffered channel of outbound messages.
	buffer chan []byte
	user   string
	cwd    []string
	host   string
	path   []string
	state  int32
}

type Response struct {
	PS1      string             `json:"ps1"`
	Commands []internal.Command `json:"commands"`
	Host     string             `json:"host"`
}

func NewTerminal(conn *websocket.Conn, os *UOS) internal.Terminal {
	term := &terminal{
		conn:   conn,
		os:     os,
		buffer: make(chan []byte, maxMessageSize),
		path:   []string{"/bin", "/usr/bin"},
		user:   "sixler",
		host:   "zellurs",
		cwd:    []string{"home", "sixler"},
	}
	cwd, _ := term.CWD()
	program.RegisterProgramsOnPath(term.LocalRoot(), append(term.path, cwd)...)

	go term.readCommands()
	go term.writeConsole()

	return term
}

func (term *terminal) readCommands() {
	defer func() {
		term.conn.Close()
	}()
	term.conn.SetReadLimit(maxMessageSize)
	term.conn.SetReadDeadline(time.Now().Add(pongWait))
	term.conn.SetPongHandler(func(string) error {
		term.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := term.conn.ReadMessage()
		if err != nil {
			break
		}
		command := bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		if err != nil {
			log.Fatal(err)
		}
		if term.hasState(internal.Authenticated) {
			term.os.RunProgram(command, term)
		} else {
			term.login(command)
		}
	}
}

func (term *terminal) login(input []byte) {
	if term.hasState(internal.UserProvided) {
		if string(input) == internal.Config.TerminalPassword {
			term.AddState(internal.Authenticated)
			term.SendActions(action.CLEAR, action.TOGGLEOBFUSCATION)
		} else {
			term.RemoveState(internal.UserProvided)
			invalid := term.Command(action.ECHO, "invalid password provided")
			toggle := term.Command(action.TOGGLEOBFUSCATION, "")
			term.SendCommands(invalid, toggle)
		}
	} else {
		if string(input) == internal.Config.TerminalUser {
			term.AddState(internal.UserProvided)
			term.SendActions(action.TOGGLEOBFUSCATION)
		} else {
			term.Send(action.ECHO, "user not found")
		}
	}
}

func (term *terminal) writeConsole() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		term.conn.Close()
		term.os.Unregister(term)
	}()
	preload := term.Command(action.PRELOAD, "./audio/keyboard-multiple.mp3")
	term.SendCommands(preload)
	for {
		select {
		case message, ok := <-term.buffer:
			term.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				term.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := term.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				log.Println(err)
				return
			}
			w.Write(message)
			// Add queued chat messages to the current websocket message.
			n := len(term.buffer)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-term.buffer)
			}
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			term.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := term.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func (term *terminal) Command(action action.Action, value string) internal.Command {
	return internal.Command{action, value}
}

func (term *terminal) Send(action action.Action, value string) {
	command := term.Command(action, value)
	term.SendCommands(command)
}

func (term *terminal) SendActions(actions ...action.Action) {
	var commands []internal.Command
	for _, action := range actions {
		command := term.Command(action, "")
		commands = append(commands, command)
	}
	term.SendCommands(commands...)
}

func (term *terminal) SendCommands(commands ...internal.Command) {
	response := Response{
		PS1:      term.PS1(),
		Commands: commands,
	}
	term.send(response)
}

func (term *terminal) LocalRoot() string {
	return term.os.localroot
}

func (term *terminal) CWD() (string, []string) {
	return string(os.PathSeparator) + strings.Join(term.cwd, string(os.PathSeparator)), term.cwd
}

func (term *terminal) Chdir(cwd []string) {
	term.cwd = cwd
}

func (term *terminal) Path() []string {
	return term.path
}

func (term *terminal) PS1() string {
	if term.hasState(internal.Authenticated) {
		cwd, _ := term.CWD()
		return fmt.Sprintf("%s@%s:%s# ", term.user, term.host, cwd)
	} else if term.hasState(internal.UserProvided) {
		return "PASSWORD: "
	}
	return "USERNAME: "
}

func (term *terminal) send(message Response) {
	response, err := json.Marshal(message)
	if err != nil {
		log.Fatal(err)
	}
	term.buffer <- response
}

func (term *terminal) AddState(state int32) {
	term.state = term.state | state
}

func (term *terminal) RemoveState(state int32) {
	term.state = term.state & ^state
}

func (term *terminal) hasState(state int32) bool {
	return term.state&state != 0
}
