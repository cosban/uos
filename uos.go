package uos

import (
	"log"
	"sync/atomic"

	"gitlab.com/cosban/uos/action"
	"gitlab.com/cosban/uos/internal"
	"gitlab.com/cosban/uos/program"
)

const (
	maxConcurrentProcesses = 32
)

// UOS represents the main
type UOS struct {
	localroot string

	// Inbound messages from the clients.
	broadcast chan string

	// Commands to process
	program chan *runner

	// Register requests from the clients.
	register chan internal.Terminal

	// Unregister requests from clients.
	unregister chan internal.Terminal

	// Active Terminals
	terminals map[internal.Terminal]bool

	// // Active Mounts
	// mounts map[*Mount]bool

	// the queue of processes
	queue []*runner

	// wait group for processes
	wg int32
}

type runner struct {
	program  string
	terminal internal.Terminal
}

func NewOS(localroot string) *UOS {
	return &UOS{
		localroot: localroot,
		terminals: make(map[internal.Terminal]bool),
		// mounts:     make(map[*Mount]bool),
		broadcast:  make(chan string),
		register:   make(chan internal.Terminal),
		unregister: make(chan internal.Terminal),
		program:    make(chan *runner),
		wg:         0,
	}
}

func (os *UOS) Run() {
	for {
		select {
		case terminal := <-os.register:
			os.terminals[terminal] = true
		case terminal := <-os.unregister:
			os.kill(terminal)
		case message := <-os.broadcast:
			for terminal := range os.terminals {
				terminal.Send(action.ECHO, string(message))
			}
		case runner := <-os.program:
			if atomic.LoadInt32(&os.wg) < maxConcurrentProcesses {
				atomic.AddInt32(&os.wg, 1)
				go func() {
					defer atomic.AddInt32(&os.wg, -1)
					program.RunProgram(runner.program, runner.terminal)
				}()
			} else {
				os.queue = append(os.queue, runner)
			}
		default:
			if atomic.LoadInt32(&os.wg) < maxConcurrentProcesses {
				atomic.AddInt32(&os.wg, 1)
				if len(os.queue) < 1 {
					atomic.AddInt32(&os.wg, -1)
				} else {
					var runner *runner
					runner, os.queue = os.queue[0], os.queue[1:]
					go func() {
						defer atomic.AddInt32(&os.wg, -1)
						program.RunProgram(runner.program, runner.terminal)
					}()
				}
			} else if len(os.queue) < 1 {
				log.Println("waiting for resources")
			}
		}
	}
}

func (os *UOS) kill(terminal internal.Terminal) {
	if _, ok := os.terminals[terminal]; ok {
		delete(os.terminals, terminal)
	}
}

func (os *UOS) Unregister(terminal internal.Terminal) {
	log.Println("terminal died")
	os.unregister <- terminal
}

func (os *UOS) Register(terminal internal.Terminal) {
	log.Println("terminal born")
	os.register <- terminal
}

func (os *UOS) RunProgram(program []byte, terminal internal.Terminal) {
	os.program <- &runner{program: string(program), terminal: terminal}
}

func (os *UOS) Broadcast(message string) {
	os.broadcast <- message
}
